package clients;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import services.LongWorks;

public class LongWorksClient {

    LongWorks ejb;

    @Before
    public void setUp() {
        ejb = getEjb();

    }

    @Test(timeout = 2000)
    public void testLongWork() {
        ejb.aLongWork();
    }

    @Test(timeout = 10000)
    public void testLongWorkWithResult() throws InterruptedException, ExecutionException {
        Future<String> future1 = ejb.aLongWorkWithResult("foo");
        Future<String> future2 = ejb.aLongWorkWithResult("foo");
        Future<String> future3 = ejb.aLongWorkWithResult("foo");
        assertEquals("foo", future1.get());
        assertEquals("foo", future2.get());
        assertEquals("foo", future3.get());
    }

    @Test
    public void testLongWorkWithResultPooling() throws InterruptedException, ExecutionException {
        Future<String> future = ejb.aLongWorkWithResult("foo");

        int numberOfPools = 0;
        while (!future.isDone()) {
            numberOfPools++;
            Thread.sleep(1000);
        }
        assertEquals("foo", future.get());
        assertEquals(6, numberOfPools);
    }

    @Test
    public void testALongBreakableWorkWithResult() throws InterruptedException {
        Future<String> future = ejb.aLongWorkWithResult("foo");
        Thread.sleep(1000);
        future.cancel(true);
    }

    private LongWorks getEjb() {
        Object ref = null;
        // prepare client context
        try {
            Context context = new InitialContext();
            // lookup EJB
            ref = context.lookup("LongWorksLocalBean");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
        // test and use it
        assertTrue(ref instanceof LongWorks);
        LongWorks ejb = (LongWorks) ref;
        return ejb;
    }
}
