package clients;

import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.EJBAccessException;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Before;
import org.junit.Test;

import services.Names;

public class NamesJaasClient {

    @EJB
    Names ejb;

    @Before
    public void setUp() throws NamingException {
        // this fail when running all the tests.
        // It's due to the mix of different way to use the EJBs and
        // some state persisting between tests and causing these issues...
        EJBContainer.createEJBContainer().getContext().bind("inject", this);
    }

    @Test
    public void testGetNameIsPublic() {
        ejb.getNames();
    }
    
    @Test(expected=EJBAccessException.class)
    public void testAddNameIsntPublic() throws Exception {
        ejb.addName("Jane Doe");
    }

    @Test
    public void testAddNameAsAnEmployee() throws Exception {
        Context context = getContext("jane-doe" , "jane passphrase");
        try {
            ejb.addName("Jane Doe");
        } finally {
            context.close();
        }
    }

    @Test(expected=EJBAccessException.class)
    public void testEmployeeCantDelete() throws Exception {
        Context context = getContext("jane-doe" , "jane passphrase");
        try {
            ejb.deleteName("Jane Doe");
        } finally {
            context.close();
        }
    }

    @Test
    public void testManagerCanDelete() throws Exception {
        Context context = getContext("alice" , "alice passphrase");
        try {
            ejb.deleteName("Jane Doe");
        } finally {
            context.close();
        }
    }

    private Context getContext(String user, String pass) throws NamingException {
        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.core.LocalInitialContextFactory");
        p.setProperty("openejb.authentication.realmName", "ServiceProviderLogin");
        p.put(Context.SECURITY_PRINCIPAL, user);
        p.put(Context.SECURITY_CREDENTIALS, pass);

        return new InitialContext(p);
    }
}
