package clients;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import javax.ejb.EJBAccessException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import services.Names;

public class NamesClient {

    Names ejb;
    Context context;

    @Before
    public void setUp() {
        ejb = getEjb(null);
    }

    /**
     * Needed because otherwise context will be shared. It might use a singleton or something else.
     * Each individual test would work. But when running all. It remembers being logged as Paul.
     * And testAddNameIsntPublic don't throw an exception
     */
    @After
    public void tearDown() throws NamingException {
        context.close();
    }

    @Test
    public void testGetNameIsPublic() {
        ejb.getNames();
    }

    @Test(expected=EJBAccessException.class)
    public void testAddNameIsntPublic() throws Exception {
        ejb.addName("Jane Doe");
    }

    @Test
    public void testAddNameAsAnEmployee() throws Exception {
        Names namesAsAnEmployee = getEjbWithRole("paul", "mot-de-passe-de-paul");
        namesAsAnEmployee.addName("Jane Doe");
    }

    @Test(expected=EJBAccessException.class)
    public void testEmployeeCantDelete() throws Exception {
        Names namesAsAnEmployee = getEjbWithRole("paul", "mot-de-passe-de-paul");
        namesAsAnEmployee.deleteName("Jane Doe");
    }

    @Test
    public void testManagerCanDelete() throws Exception {
        Names namesAsAManager = getEjbWithRole("pierre", "mot-de-passe-de-pierre");
        namesAsAManager.deleteName("Jane Doe");
    }

    private Names getEjbWithRole(String loginHandle, String password) {
        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY,
            "org.apache.openejb.core.LocalInitialContextFactory");
        p.put(Context.SECURITY_PRINCIPAL, loginHandle);
        p.put(Context.SECURITY_CREDENTIALS, password);
        Names namesAsAnEmployee = getEjb(p);
        return namesAsAnEmployee;
    }

    private Names getEjb(Properties props) {
        Object ref = null;
        // prepare client context
        try {
            context = new InitialContext(props); // stored so we can close it
            // lookup EJB
            ref = context.lookup("NamesLocalBean");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
        // test and use it
        assertTrue(ref instanceof Names);
        Names ejb = (Names) ref;
        return ejb;
    }
}
