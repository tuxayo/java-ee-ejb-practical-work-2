package clients;

import static org.junit.Assert.assertTrue;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Before;
import org.junit.Test;

import services.AsManager;
import services.Names;

public class AsManagerClient {

    AsManager asManagerEjb;
    Names namesEjb;

    @Before
    public void setUp() {
        asManagerEjb = getAsManagerEjb();
        namesEjb = getNamesEjb();
    }

    @Test
    public void testDeleteNameIsAccessible() throws Exception {
        asManagerEjb.deleteName(namesEjb, "Jane Doe");
    }

    private AsManager getAsManagerEjb() {
        Object ref = null;
        // prepare client context
        try {
            Context context = new InitialContext();
            // lookup EJB
            ref = context.lookup("AsManagerLocalBean");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
        // test and use it
        assertTrue(ref instanceof AsManager);
        AsManager ejb = (AsManager) ref;
        return ejb;
    }

    private Names getNamesEjb() {
        Object ref = null;
        // prepare client context
        try {
            Context context = new InitialContext();
            // lookup EJB
            ref = context.lookup("NamesLocalBean");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
        // test and use it
        assertTrue(ref instanceof Names);
        Names ejb = (Names) ref;
        return ejb;
    }
}
