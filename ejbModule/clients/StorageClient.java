package clients;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import services.Storage;

public class StorageClient {
    Storage ejb;

    @Before
    public void setUp() {
        ejb = getEjb();

    }

    @Test
    public void testReadAccess() throws Exception {
        Runnable executionRead = () -> {
            System.out.println(ejb.get("foo"));
        };

        Runnable executionWrite = () -> {
            ejb.set("foo", "bar");
        };

        ExecutorService exec = Executors.newFixedThreadPool(10);
        long begin = System.nanoTime();
        exec.execute(executionRead);
        exec.execute(executionWrite);
        exec.execute(executionRead);
        // attente de la fin des threads
        exec.shutdown();
        exec.awaitTermination(10, TimeUnit.HOURS);
        long end = System.nanoTime();
        long durationNano = end - begin;
        long durationMs = durationNano / 1_000_000;
        long MIN = 4800;
        long MAX = 5200;
        // no lock → 3 sec
        // full mutual exclusion → 7 sec
        // expected → 5 sec
        boolean isDurationInRange = MIN <= durationMs && durationMs <= MAX;
        assertTrue(
                "duration is out of range: " + durationMs + " unfortunately it can fail randomly...",
                isDurationInRange);
        System.out.println("duration: " + durationMs);
    }

    private Storage getEjb() {
        Object ref = null;
        // prepare client context
        try {
            Context context = new InitialContext();
            // lookup EJB
            ref = context.lookup("storageLocalBean");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
        // test and use it
        Assert.assertTrue(ref instanceof Storage);
        Storage ejb = (Storage) ref;
        return ejb;
    }

}
