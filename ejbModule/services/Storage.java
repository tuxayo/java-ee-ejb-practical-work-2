package services;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert;

@Singleton(name = "storage")
@Startup()
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@DependsOn("logger")
public class Storage {

    final Map<String, String> storage;

    public Storage() {
        storage = new HashMap<String, String>();
    }

    @PostConstruct
    public void init() {
        // keep that to see if it inits before the logger
        // logger should init before
        System.out.println("Storage running...");
        Logger logger = getLogger();
        logger.log("Storage running with logger");
    }

    /* Récupérer une valeur (utilisations simultanées) */
    @Lock(LockType.READ)
    public String get(String key) {
        sleep(2000);
        return storage.get(key);
    }

    /* Déposer une valeur (utilisation exclusive) */
    @Lock(LockType.WRITE)
    public void set(String key, String value) {
        sleep(3000);
        storage.put(key, value);
    }

    private void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    
    private Logger getLogger() {
        Object ref = null;
        // prepare client context
        try {
            Context context = new InitialContext();
            // lookup EJB
            ref = context.lookup("loggerLocalBean");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
        // test and use it
        Assert.assertTrue(ref instanceof Logger);
        Logger ejb = (Logger) ref;
        return ejb;
    }

}