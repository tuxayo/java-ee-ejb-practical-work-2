package services;

import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

@Stateless
public class LongWorks {

    @Resource
    private SessionContext context;

    @PostConstruct
    public void prepare() {
        System.out.println("Preparation de " + this);
    }

    @Asynchronous
    public void aLongWork() {
        for (int i = 0; (i < 10); i++) {
            sleep(1000);
        }
    }
    
    @Asynchronous
    public Future<String> aLongWorkWithResult(String value) {
        sleep(5000);
        return new AsyncResult<String>(value);
    }
    
    @Asynchronous
    public Future<String> aLongBreakableWorkWithResult(String value) {
        for (int i = 0; (i < 15); i++) {
            sleep(500);
            if (context.wasCancelCalled()) {
                return null;
            }
        }
        return new AsyncResult<String>(value);
    }

    private void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
        }
    }

}