package services;

import javax.annotation.security.RunAs;
import javax.ejb.Stateless;

@Stateless
@RunAs("Manager")
public class AsManager {

    public void deleteName(Names names, String name) {
        names.deleteName(name);
    }

}