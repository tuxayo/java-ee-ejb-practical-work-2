package services;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;

@Stateful
public class Names {

    @Resource
    SessionContext sessionContext;

    Set<String> names = new java.util.HashSet<>();

    @RolesAllowed({ "Employee", "Manager" })
    public void addName(String name) throws Exception {
        if(sessionContext.isCallerInRole("Employee")) {
            System.out.println("Caller is Employee");
        } else if (sessionContext.isCallerInRole("Manager")) {
            System.out.println("Caller is Manager");
        } else {
            throw new RuntimeException("WTF is called then???");
        }
        names.add(name);
    }

    @RolesAllowed({ "Manager" })
    public void deleteName(String name) {
        if (sessionContext.isCallerInRole("Manager")) {
            System.out.println("Caller is Manager");
        } else {
            throw new RuntimeException("WTF is called then???");
        }
        names.remove(name);
    }

    @PermitAll
    public List<String> getNames() {
        return new LinkedList<>(names);
    }
}