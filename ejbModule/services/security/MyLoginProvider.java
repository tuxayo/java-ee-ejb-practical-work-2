package services.security;

import org.apache.openejb.core.security.jaas.LoginProvider;

import javax.security.auth.login.FailedLoginException;
import java.util.Arrays;
import java.util.List;

public class MyLoginProvider implements LoginProvider {


    @Override
    public List<String> authenticate(String user, String password) throws FailedLoginException {
        if ("jane-doe".equals(user) && "jane passphrase".equals(password)) {
            return Arrays.asList("Employee");
        }

        if ("alice".equals(user) && "alice passphrase".equals(password)) {
            return Arrays.asList("Employee", "Manager");
        }

        throw new FailedLoginException("Bad user or password!");
    }
}