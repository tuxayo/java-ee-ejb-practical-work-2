package services;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

@Singleton(name = "logger")
public class Logger {

    @PostConstruct
    public void init() {
        System.out.println("Logger ready");
    }
    
    public void log(String message) {
        System.out.println(message);
    }
}
