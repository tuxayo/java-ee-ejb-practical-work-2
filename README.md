# Specs
http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/jee/tp-EJB2.html
http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/jee/tp-EJB-JPA.html

# OpenEJB Setup (for Eclipse)
## 1. Download OpenEJB Standalone
https://tomee.apache.org/downloads.html

The code was tested with version 4.7.4

## 2. Extract it

## 3. Use it as runtime in Eclipse for the project
You need OpenEJB plugin

https://openejb.apache.org/installation.html

# DB setup


## Create DB
CREATE DATABASE mydbname;

## Create User
GRANT ALL PRIVILEGES on mydbname.* to 'myuser'@'localhost' IDENTIFIED BY 'mypass';

Copy the sample file and edit the values according to your database

`cp conf/openejb.xml.sample conf/openejb.xml`

Fill the config file with the values according to your DB
